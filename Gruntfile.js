'use strict';

module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-webfont');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({

        stylus: {
            main: {
                options: {
                    compress: true,
                    paths: ['src/styl/**/*.styl'],
                    relativeDest: 'dist/css',
                    sourcemap: {
                        inline: true
                    }
                },
                files: {
                    'stylus.css': ['src/styl/stylistic.styl']
                }
            }
        },

        babel: {
            main: {

            }
        },

        webfont: {
            main: {
                src: 'src/icons/*.svg',
                dest: 'dist/fonts',
                destCss: 'src/styl',
                options: {
                    stylesheet: 'styl',
                    template: 'src/icons/_icons.styl',
                    relativeFontPath: 'src/fonts',
                    engine: 'node',
                    fontFamilyName: 'stylfont',
                    fontFilename: 'stylfont',
                    htmlDemo: false,
                    templateOptions: {
                        baseClass: 'glyph-icon',
                        classPrefix: 'glyph_',
                        mixinPrefix: 'glyph-',
                        fontPath: '.fonts',
                        fontName: 'stylfont'
                    }
                }
            }
        }

    });

    grunt.registerTask('default', ['stylus', 'webfont']);

}